<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'two-wiki' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'mysql' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'n6?NsLqSh?-Bp`e Z9@x8R/ko#/H}^S,2v|Lp|CcdVWH=3RR[U<O{47G3G:uH<{J' );
define( 'SECURE_AUTH_KEY',  '7`#%V]8PgM=)3&.;-xic>xVZ= UGY}nYgjQv,xYBQUQE?{?uG3mDJMz=OIa@fIdY' );
define( 'LOGGED_IN_KEY',    '&l_ZBQGA&ZnotAN.<a8K4ww6eX+j1!Q^>v^GRnyNOpxwpVV],v*$_|)xu:88Q/O3' );
define( 'NONCE_KEY',        'S_{>57b{htl{|HIMT-of&[z)HJOX5(|*y[6TInnNoO(k6+r.(0>W;hEsZiCib8<`' );
define( 'AUTH_SALT',        '6?9p[rbVqiBSU{O3YoB4F V{782;t]%n9{p3Ed~x:BZt?[+UjPna!Eq}%k#mlMG7' );
define( 'SECURE_AUTH_SALT', ' LB;<W40WfjxClP5#,nz)?W#L|(j@&&Et`5I%`Bguqz@N=Yf_p3lJMO`dTcbAuqS' );
define( 'LOGGED_IN_SALT',   'B_Sx_*,;aDlI<i|beW8*12x J4v#0T8Dq@,FJ+%x!O-z7hbf+uWE/KU;v9C.x|i=' );
define( 'NONCE_SALT',       'G-u?5 ^n][D$aPBa<?v.Ogl{-[hsF-UR1yn@:x BrC$r>vd(5#_Z]PX-&?lPz=uk' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'two_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
